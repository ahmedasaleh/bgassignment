/*
 * InputFormatValidatore.cpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : validates that the input adheres to our expected format.
 */

#include "InputFormatValidator.hpp"

namespace bgassignment {

InputFormatValidator::InputFormatValidator() {
	readConfiguration(_pathToConfiguration);

}

InputFormatValidator::~InputFormatValidator() {
	// TODO Auto-generated destructor stub
}

/*
 * @description: this method reads configuration file holds supported input formats.
 * 				Configuration for each format is written in tuple: delimiter ascii code,
 * 				number of tokens, name for the format
 * @param pathToConfiguration configuration file path and name
 * @return return whether read was successful or failed
 */
int InputFormatValidator::readConfiguration(string pathToConfiguration) {
	int _status = Commons::ERROR;
	string _temp; //temp storage to convert chr* to string
	char line[_lineSize]; //create storage for the characters which will be read
	ifstream _inFile(pathToConfiguration.c_str(), ifstream::in); //Open as stream for read
	if (!_inFile) {
		return Commons::FILE_NOT_FOUND;
	}
	while (_inFile.good()) {
		_inFile.getline(line, _lineSize - 1); //read line into temporary storage
		string _delimiter, _numberOfTokens, _formatName; //temp storage
		tuple _tuple; //our tuple
		stringstream ss(stringstream::in | stringstream::out);
		ss.str(line);
		ss >> _delimiter >> _numberOfTokens >> _formatName; //parse line to group of strings
		_tuple._delimiter = stoi(_delimiter);
		_tuple._numberOfTokens = stoi(_numberOfTokens);
		_tuple._formatName = _formatName;
		_inputFormats.push_back(_tuple);
		_status = Commons::SUCCESS;
	}
	_inFile.close();
	return _status;
}

/*
 * @description: this method validates if an input string adheres to our supported format
 * 				and gives name of that format
 * @param input string to be validated
 * @param type reference to variable holds detected format
 * @return return whether found supported or unsupported format
 */
int InputFormatValidator::validateInput(string input, string &type) {
	bool _status = Commons::UNSUPPORTED_FORMAT_FOUND;
	string _temp = input;
	//check if any of our supported delimiters exists in the string
	for (tuple format : _inputFormats) {
		unsigned int _delimiterCount = countDelimiterOccurrences(_temp,	format._delimiter);
		//if delimiter count adheres to our format continue processing
		if (_delimiterCount == format._numberOfTokens - 1) {
			//count number of tokens
			if ((int) format._delimiter == Commons::SPACE_CHAR) {
				unsigned int _tokensCount = countTokens(_temp);
				if(_tokensCount == format._numberOfTokens){
					type = format._formatName;
					_status = Commons::SUPPORTED_FORMAT_FOUND;
					return _status;
				}
			} else if((int) format._delimiter == Commons::PIPE_CHAR){
				consumeWhiteSpaces(_temp);
				std::replace(_temp.begin(),_temp.end(),format._delimiter,_spaceChar);
				unsigned int _tokensCount = countTokens(_temp) ;
				if(_tokensCount == format._numberOfTokens){
					type = format._formatName;
					_status = Commons::SUPPORTED_FORMAT_FOUND;
					return _status;
				}

			} else if((int) format._delimiter == Commons::COMMA_CHAR){
				consumeWhiteSpaces(_temp);
				std::replace(_temp.begin(),_temp.end(),format._delimiter,_spaceChar);
				unsigned int _tokensCount = countTokens(_temp) ;
				if(_tokensCount == format._numberOfTokens){
					type = format._formatName;
					_status = Commons::SUPPORTED_FORMAT_FOUND;
					return _status;
				}

			}
		}

	}
	return _status;
}

/*
 * @description: counts number of specific delimiter in input string
 * @param inputString string to be validated
 * @param delimiter delimiter
 * @return return count
 */
unsigned int InputFormatValidator::countDelimiterOccurrences(string inputString,	char delimiter) {
	unsigned int _delimiterCount = 0;
	for (int i = 0; i < inputString.size(); i++) {
		if (inputString[i] == delimiter) {
			_delimiterCount++;
		}
	}
	return _delimiterCount;
}

/*
 * @description: counts number tokens in input string
 * @param inputString string to be counted
 * @return return count
 */
unsigned int InputFormatValidator::countTokens(string inputString) {
	unsigned int _tokensCount = 0;
	for (int i = 0; i < inputString.length(); i++) {
		if ((int) inputString[i] == Commons::SPACE_CHAR) //Checking for spaces
				{
			_tokensCount++;
		}
	}
	return ++_tokensCount;
}
/*
 * @description: removes white spaces in string
 * @param inputString string to be cleaned
 * @return return success or fail
 */

bool InputFormatValidator::consumeWhiteSpaces(string &inputString){
	bool status = false;
	string _token;
	string _streamWithNoSpaces;
	if(inputString.empty()){
		status = false;
		return status;
	}
	status = true;
	stringstream ss(stringstream::in | stringstream::out );
	ss.str( inputString );
	while(ss >> _token){
		_streamWithNoSpaces += _token;
	}
	inputString = _streamWithNoSpaces;
	return status;
}

} /* namespace bgassignment */
