/*
 * LastNameSort.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting by birth date asc then by name asc
 */

#include "BirthLastnameAscSort.hpp"

#include <set>
#include "PersonInformation.hpp"
namespace bgassignment {

BirthLastnameAscSort::BirthLastnameAscSort() {
	// TODO Auto-generated constructor stub

}

BirthLastnameAscSort::~BirthLastnameAscSort() {
	// TODO Auto-generated destructor stub
}

/*
 * @Description sort persons by gender putting females first
 * @param personsInformation (input) all persons data
 * @
 */

void BirthLastnameAscSort::sortPersons(vector<PersonInformation>  personsInformation){
	set<PersonInformation, BirthAsc> _birthOut2;
	set<PersonInformation, BirthAsc>::iterator iter_b;

	vector<PersonInformation> _personsInformation = personsInformation;
	//fill set with the person info and it will sort it based on the BirthAsc comparator
	vector<PersonInformation>::iterator all_iter,all_end;
	for(all_iter =  _personsInformation.begin(),all_end = _personsInformation.end();all_iter != all_end; ++all_iter){
		map<string, string> _personMap;//single person information will be here
		(all_iter)->getPersonInformation(_personMap);
		PersonInformation _pi = PersonInformation(_personMap);
		_birthOut2.insert(_pi);
	}
	cout<< "\nOutput 2:\n\n";
	for (iter_b = _birthOut2.begin(); iter_b != _birthOut2.end(); ++iter_b) {
		std::cout << (iter_b)->printPersonInformation() << std::endl;
	}

}

} /* namespace bgassignment */
