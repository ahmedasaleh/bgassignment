/*
 * PipeParser.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 */

#ifndef PIPEPARSER_HPP_
#define PIPEPARSER_HPP_

#include "IParser.hpp"
#include "PersonInformation.hpp"
namespace bgassignment {

class PipeParser: public IParser {
public:
	PipeParser();
	virtual ~PipeParser();
	//vector<string> parse(vector<string>  &personsInformation);
	vector<PersonInformation> parse(vector<string>  &personsInformation);
	bool consumeWhiteSpaces(string &inputString, string &inputStringNoSpaces);

private:
	const char _spaceChar = Commons::SPACE_CHAR;
};


} /* namespace bgassignment */

#endif /* PIPEPARSER_HPP_ */
