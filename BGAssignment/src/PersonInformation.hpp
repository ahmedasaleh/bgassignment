/*
 * PersonInformation.hpp
 *
 *  Created on: Oct 3, 2016
 *      Author: ahmedasaleh
 *  Description : person entity class.
 */

#ifndef PERSONINFORMATION_HPP_
#define PERSONINFORMATION_HPP_

#include "Commons.hpp"
#include <map>
#include <algorithm>			// std::replace

namespace bgassignment {

class PersonInformation {
public:
	string printPersonInformation() const;
	void getPersonInformation(map<string, string> &personInformation) ;
	PersonInformation();
	PersonInformation(string lastName,string gender,string birthdate);
	PersonInformation(string lastName,string firstName,string middleInitial,string gender,string birthdate,string favoriteColor);
	PersonInformation(string lastName,string firstName,string gender,string birthdate,string favoriteColor);
	PersonInformation(map<string, string> personInformation);
	virtual ~PersonInformation();
	void setLastName(string lastName);
	void setFirstName(string firstName);
	void setGender(string gender);
	void setFavoriteColor(string favoriteColor);
	void setDateOfBirth(string dateOfBirth);
	void setMiddleInitial(string middleInitial);
	string getLastName( ) const;
	string getFirstName( ) const;
	string getGender( ) const;
	string getFavoriteColor( ) const;
	string getDateOfBirth( ) const;
	string getMiddleInitial( ) const;
	void getDateOfBirth(int &day,int &month,int &year) const;
	// We need the operator== to compare 2 PersonInformation types.
	    bool operator=(const PersonInformation &other) const;
private:
		string _firstName;
		string _lastName;
		string _gender;
		string _favoriteColor;
		string _dateOfBirth;
		string _middleInitial;

	string reformatBirthDate(string birthDate);
	string reformatGender(string gender);
	const char _separator=Commons::SPACE_CHAR;
	map<string, string> _personInformation;
	int _dayOfBirth, _monthOfBirth, _yearOfBirth;
};

} /* namespace bgassignment */

#endif /* PERSONINFORMATION_HPP_ */
