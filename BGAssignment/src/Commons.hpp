/*
 * Commons.hpp
 *
 *  Created on: Oct 1, 2016
 *      Author: ahmedasaleh
 *  Description : This file holds all static variables and common headers.
 */

#ifndef COMMONS_HPP_
#define COMMONS_HPP_

#include <vector>
#include <string>
#include <fstream>			// std::ifstream
#include <iostream>			// std::cout
#include <stdexcept>
#include <sstream>			// std::stringstream
#include <algorithm>			// std::replace
#include <set>
using namespace std;

namespace bgassignment {

class Commons {
public:
	static const int SPACE_CHAR = 0x20; //space char in hexa
	static const int PIPE_CHAR = 0x7c; //pipe char in hexa
	static const int COMMA_CHAR = 0x2c; //comma char in hexa

	static const int SUCCESS = 0;
	static const int ERROR = 1;
	static const int FILE_NOT_FOUND = 2;
	static const int SUPPORTED_FORMAT_FOUND = 3;
	static const int UNSUPPORTED_FORMAT_FOUND = 4;

	static const std::string PIPE_TYPE;
	static const std::string SPACE_TYPE;
	static const std::string COMMA_TYPE;

};


} /* namespace bgassignment */

#endif /* COMMONS_HPP_ */
