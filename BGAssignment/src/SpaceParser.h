/*
 * SpaceParser.h
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 */

#ifndef SPACEPARSER_H_
#define SPACEPARSER_H_

#include "IParser.hpp"
#include "PersonInformation.hpp"
namespace bgassignment {

class SpaceParser: public IParser {
public:
	SpaceParser();
	virtual ~SpaceParser();
	//vector<string> parse(vector<string>  &personsInformation);
	vector<PersonInformation> parse(vector<string>  &personsInformation);
private:
	const char _spaceChar = Commons::SPACE_CHAR;

};

} /* namespace bgassignment */

#endif /* SPACEPARSER_H_ */
