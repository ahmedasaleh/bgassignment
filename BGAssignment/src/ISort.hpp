/*
 * ISort.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : This is the the interface file for sorting strategies
 */

#ifndef ISORT_HPP_
#define ISORT_HPP_
#include <algorithm>			// std::replace
#include "PersonInformation.hpp"
#include "Commons.hpp"
namespace bgassignment {

class ISort {
public:
	virtual void sortPersons(vector<PersonInformation>  personsInformation)=0;
private:
	vector<string> _sortedOutput;
};

} /* namespace bgassignment */

#endif /* ISORT_HPP_ */
