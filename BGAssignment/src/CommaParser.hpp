/*
 * CommaParser.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : This is the class responsible for parsing based on comma separation.
 *  It creates PersonInformation object to be handled subsequently
 */

#ifndef COMMAPARSER_HPP_
#define COMMAPARSER_HPP_

#include "IParser.hpp"

namespace bgassignment {

class CommaParser: public IParser {
public:
	CommaParser();
	virtual ~CommaParser();
	vector<PersonInformation> parse(vector<string>  &personsInformation);
	bool consumeWhiteSpaces(string &inputString, string &outputStringNoSpaces);
private:
	const char _spaceChar = Commons::SPACE_CHAR;
};

} /* namespace bgassignment */

#endif /* COMMAPARSER_HPP_ */
