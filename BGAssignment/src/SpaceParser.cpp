/*
 * SpaceParser.cpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : This is the class responsible for parsing based on space separation.
 *  It creates PersonInformation object to be handled subsequently
 */

#include "SpaceParser.h"

namespace bgassignment {

SpaceParser::SpaceParser() {
	_delimiter = _spaceChar;

}

SpaceParser::~SpaceParser() {
	// TODO Auto-generated destructor stub
}

/*
 * @Description parse input streem delimited by space
 * @param _personsInformation (input) all persons data as string space delimited
 * @return return person information objects in a vector container
 */
vector<PersonInformation> SpaceParser::parse(vector<string> &_personsInformation) {
	vector<PersonInformation> _personsparsed;
	for (string _personInformation : _personsInformation) {//LastName FirstName MiddleInitial Gender DateOfBirth FavoriteColor
		string _lastName, _firstName, _middleInitial, _gender, _dateOfBirth, _favoriteColor;
		stringstream ss ( stringstream::in | stringstream:: out );
		ss.str(_personInformation);
		ss >> _lastName >> _firstName >> _middleInitial >> _gender >> _dateOfBirth >> _favoriteColor;
		PersonInformation _person =  PersonInformation(_lastName, _firstName, _middleInitial, _gender, _dateOfBirth, _favoriteColor);
		_personsparsed.push_back(_person);
	}

	return _personsparsed;
}

} /* namespace bgassignment */
