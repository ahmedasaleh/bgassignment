/*
 * InputFormatValidatore.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : validates that the input adheres to our expected format.
 */

#ifndef INPUTFORMATVALIDATOR_HPP_
#define INPUTFORMATVALIDATOR_HPP_
#include "Commons.hpp"
namespace bgassignment {

class InputFormatValidator {
public:
	InputFormatValidator();
	virtual ~InputFormatValidator();
	int validateInput(string input,string &type);
	string getFormatType();
private:
	int readConfiguration(string pathToConfiguration);
	bool consumeWhiteSpaces(string &inputString);
	unsigned int countDelimiterOccurrences(string inputString,char delimiter);
	unsigned int countTokens(string inputString);
	const string _pathToConfiguration = "../BGAssignment/src/inputFormats.txt";
	const int _lineSize = 84;
	char _delimiter;
	struct tuple {
	    char _delimiter;
	    int _numberOfTokens;
	    string _formatName;
	  };
	vector<tuple> _inputFormats;
	const char _spaceChar = Commons::SPACE_CHAR;

};

} /* namespace bgassignment */

#endif /* INPUTFORMATVALIDATOR_HPP_ */
