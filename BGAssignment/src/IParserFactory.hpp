/*
 * IParserFactory.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : parser factory interface.
 */

#ifndef IPARSERFACTORY_HPP_
#define IPARSERFACTORY_HPP_
#include "IParser.hpp"

namespace bgassignment {

class IParserFactory {
public:
	virtual IParser *createParser(string parserType) = 0;
};

} /* namespace bgassignment */

#endif /* IPARSERFACTORY_HPP_ */
