/*
 * IParser.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : parser interface.
 *
 */

#ifndef IPARSER_HPP_
#define IPARSER_HPP_


#include "Commons.hpp"
#include "PersonInformation.hpp"
namespace bgassignment {

class IParser {
public:
	virtual vector<PersonInformation> parse(vector<string>  &personsInformation)=0;
	bool consumeWhiteSpaces(string &inputString, string &inputStringNoSpaces);

protected:
	vector<string> _personsInformation;
	char _delimiter;
};

} /* namespace bgassignment */

#endif /* IPARSER_HPP_ */
