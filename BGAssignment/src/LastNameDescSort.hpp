/*
 * LastNameDescSort.hpp
 *
 *  Created on: Oct 4, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting desc by last name
 */

#ifndef LASTNAMEDESCSORT_HPP_
#define LASTNAMEDESCSORT_HPP_

#include "ISort.hpp"

namespace bgassignment {

class LastNameDescSort: public ISort {
public:
	LastNameDescSort();
	virtual ~LastNameDescSort();
	void sortPersons(vector<PersonInformation>  personsInformation);
	class LastNameDesc {
	public:
		bool operator()(const PersonInformation& s1, const PersonInformation& s2) {
			return s1.getLastName().compare(s2.getLastName()) > 0;
		}
	};
};

} /* namespace bgassignment */

#endif /* LASTNAMEDESCSORT_HPP_ */
