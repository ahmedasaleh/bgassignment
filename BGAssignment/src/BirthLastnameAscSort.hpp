/*
 * LastNameSort.hpp
 *
 *  Created on: Oct 4, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting by birth date asc then by name asc
 */

#ifndef BIRTHLASTNAMEASCSORT_HPP_
#define BIRTHLASTNAMEASCSORT_HPP_

#include "ISort.hpp"

namespace bgassignment {

class BirthLastnameAscSort: public ISort {
public:
	BirthLastnameAscSort();
	virtual ~BirthLastnameAscSort();
	void sortPersons(vector<PersonInformation>  personsInformation);

	class BirthAsc {
	public:
		bool operator()(const PersonInformation& s1, const PersonInformation& s2) {
			int s1_day_int =0,s1_month_int=0,s1_year_int=0,s2_day_int=0,s2_month_int=0,s2_year_int=0;
			s1.getDateOfBirth(s1_day_int, s1_month_int, s1_year_int);
			s2.getDateOfBirth(s2_day_int, s2_month_int, s2_year_int);

			if((s1_year_int<s2_year_int)){
				return true;
				if(s1_month_int<s2_month_int){
					if(s1_day_int<s2_day_int){
						if(s1.getLastName().compare(s2.getLastName())){
							return true;
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}

				}
				else if(s1_month_int==s2_month_int){
					if(s1_day_int<s2_day_int){
						if(s1.getLastName().compare(s2.getLastName())){
							return true;
						}
						else{
							return false;
						}
					}
					else{
						return false;
					}
				}

			}
			else if(s1_year_int==s2_year_int){
				if(s1_month_int<s2_month_int){
					if(s1_day_int<s2_day_int){
						if(s1.getLastName().compare(s2.getLastName())){
							return true;
						}
						else{
							return false;
						}

					}
					else{
						return false;
					}

				}
				else if(s1_month_int==s2_month_int){
					//return true;
					if(s1_day_int<s2_day_int){
					}else if(s1_day_int==s2_day_int){
						if(s1.getLastName().compare(s2.getLastName()) <0){
							return true;
						}
						else{
							return false;
						}

					}
					else{
						if(s1.getLastName().compare(s2.getLastName()) <0){
							return true;
						}
						else{
							return false;
						}

					}
				}

			}
			else{
				return false;
			}
			return false;//default value
		}
	};

private:
	set<PersonInformation> _sortedItems;
	vector<string> out;

};

} /* namespace bgassignment */

#endif /* BIRTHLASTNAMEASCSORT_HPP_ */
