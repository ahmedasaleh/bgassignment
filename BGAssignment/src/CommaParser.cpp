/*
 * CommaParser.cpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : This is the class responsible for parsing based on comma separation.
 *  It creates PersonInformation object to be handled subsequently
 */

#include "CommaParser.hpp"
#include "PersonInformation.hpp"
namespace bgassignment {

CommaParser::CommaParser() {
	_delimiter = Commons::COMMA_CHAR;

}

CommaParser::~CommaParser() {
	// TODO Auto-generated destructor stub
}


/*
 * @Description parse input streem delimited by comma
 * @param _personsInformation (input) all persons data as string comma delimited
 * @return return person information objects in a vector container
 */
vector<PersonInformation> CommaParser::parse(vector<string> &_personsInformation) {
	vector<PersonInformation> _personsparsed ;
	string _personInformationNoSpaces;
	for(string _personInformation: _personsInformation){ //LastName, FirstName, Gender, FavoriteColor, DateOfBirth

		string _lastName, _firstName, _gender, _favoriteColor, _dateOfBirth;
		if(consumeWhiteSpaces(_personInformation,_personInformationNoSpaces)){
			std::replace(_personInformationNoSpaces.begin(),_personInformationNoSpaces.end(),_delimiter,_spaceChar);
			stringstream ss ( stringstream::in | stringstream:: out );
			ss.str(_personInformationNoSpaces);
			ss >> _lastName >> _firstName >> _gender >> _favoriteColor >> _dateOfBirth;
			PersonInformation _person =  PersonInformation(_lastName, _firstName, _gender, _dateOfBirth, _favoriteColor);
			_personsparsed.push_back(_person);
		}
	}

	return _personsparsed;
}

/*
 * @Description remove white spaces
 * @param inputString (input) single person data as string comma delimited
 * @param outputStringNoSpaces (output) single person data as string comma delimited with no spaces
 * @return return success or fail
 */
bool CommaParser::consumeWhiteSpaces(string &inputString, string &outputStringNoSpaces){
	bool status = false;
	string _token;
	string _streamWithNoSpaces;
	if(inputString.empty()){
		status = false;
		return status;
	}
	status = true;
	stringstream ss(stringstream::in | stringstream::out );
	ss.str( inputString );
	while(ss >> _token){
		_streamWithNoSpaces += _token;
	}
	outputStringNoSpaces = _streamWithNoSpaces;
	return status;
}

} /* namespace bgassignment */
