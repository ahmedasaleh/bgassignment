/*
 * ParserFactory.hpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : parser factory.
 */

#ifndef PARSERFACTORY_HPP_
#define PARSERFACTORY_HPP_

#include "IParserFactory.hpp"
#include "PipeParser.hpp"
#include "SpaceParser.h"
#include "CommaParser.hpp"

namespace bgassignment {

class ParserFactory: public IParserFactory {
public:
	ParserFactory();
	virtual ~ParserFactory();
	IParser* createParser(string parserType);
};

} /* namespace bgassignment */

#endif /* PARSERFACTORY_HPP_ */
