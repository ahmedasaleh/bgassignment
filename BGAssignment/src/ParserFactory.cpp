/*
 * ParserFactory.cpp
 *
 *  Created on: Oct 2, 2016
 *      Author: ahmedasaleh
 *  Description : parser factory.
 */

#include "ParserFactory.hpp"

namespace bgassignment {

ParserFactory::ParserFactory() {
	// TODO Auto-generated constructor stub

}

ParserFactory::~ParserFactory() {
	// TODO Auto-generated destructor stub
}

IParser* ParserFactory::createParser(string parserType){
	IParser* _parser;
	if(parserType.compare(Commons::PIPE_TYPE) == 0) {
		_parser =  new PipeParser;
	}
	else if(parserType.compare(Commons::COMMA_TYPE) == 0) {
		_parser =  new CommaParser;
	}
	else if(parserType.compare(Commons::SPACE_TYPE) == 0) {
		_parser = new SpaceParser;
	}

	return _parser;
}
} /* namespace bgassignment */
