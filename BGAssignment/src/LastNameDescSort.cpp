/*
 * LastNameDescSort.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting desc by last name
 */

#include "LastNameDescSort.hpp"

namespace bgassignment {

LastNameDescSort::LastNameDescSort() {
	// TODO Auto-generated constructor stub

}

LastNameDescSort::~LastNameDescSort() {
	// TODO Auto-generated destructor stub
}
/*
 * @Description sort persons desc by last name
 * @param personsInformation (input) all persons data
 * @
 */
void LastNameDescSort::sortPersons(vector<PersonInformation>  personsInformation){
	set<PersonInformation, LastNameDesc> _lastNameOut3;
	set<PersonInformation, LastNameDesc>::iterator iter_l;

	vector<PersonInformation> _personsInformation = personsInformation;
	//fill set with the person info and it will sort it based on the BirthAsc comparator
	vector<PersonInformation>::iterator all_iter,all_end;
	for(all_iter =  _personsInformation.begin(),all_end = _personsInformation.end();all_iter != all_end; ++all_iter){
		map<string, string> _personMap;//single person information will be here
		(all_iter)->getPersonInformation(_personMap);
		PersonInformation _pi = PersonInformation(_personMap);
		_lastNameOut3.insert(_pi);
	}
	cout<< "\nOutput 3:\n\n";
	for (iter_l = _lastNameOut3.begin(); iter_l != _lastNameOut3.end(); ++iter_l) {
		std::cout << (iter_l)->printPersonInformation() << std::endl;
	}

}

} /* namespace bgassignment */
