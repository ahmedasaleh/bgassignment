/*
 * GenderSort.hpp
 *
 *  Created on: Oct 3, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting by gender then by name asc
 */

#ifndef GENDERLASTNAMEASCSORT_HPP_
#define GENDERLASTNAMEASCSORT_HPP_

#include "ISort.hpp"

namespace bgassignment {

class GenderLastnameAscSort: public ISort {
public:
	GenderLastnameAscSort();
	virtual ~GenderLastnameAscSort();
	void sortPersons(vector<PersonInformation>  personsInformation);
	class LastNameAsc {
	public:
		bool operator()(const PersonInformation& s1, const PersonInformation& s2) {
			return s1.getLastName().compare(s2.getLastName()) < 0;
		}
	};

};

} /* namespace bgassignment */

#endif /* GENDERLASTNAMEASCSORT_HPP_ */
