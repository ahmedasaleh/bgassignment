//============================================================================
// Name        : BGAssignment.cpp
// Author      : AhmedASaleh
// Version     : 1.0
// Copyright   : You can use it if you like it
// Description : This is the main entry for the application
//============================================================================

#include "BirthLastnameAscSort.hpp"
#include "IParserFactory.hpp"
#include "IParser.hpp"
#include "ParserFactory.hpp"
#include "Commons.hpp"
#include "GenderLastnameAscSort.hpp"
#include "InputFormatValidator.hpp"
#include "LastNameDescSort.hpp"
using namespace bgassignment;
const string Commons::PIPE_TYPE = "pipe";
const string Commons::SPACE_TYPE = "space";
const string Commons::COMMA_TYPE = "comma";



int main() {
	//automatically detect input, whether pipe, space, or comma and create parser based on that

	vector<string> uknown_string_inputs, pipe_string_inputs,
			space_string_inputs, comma_string_inputs;
	uknown_string_inputs = {"Smith | Steve | D | M | Red | 3-3-1985","Bonk | Radek | S | M | Green | 6-3-1975","Bouillon | Francis | G | M | Blue | 6-3-1975",
		"Kournikova Anna F F 6-3-1975 Red","Hingis Martina M F 4-2-1979 Green","Seles Monica H F 12-2-1973 Black",
		"Abercrombie, Neil, Male, Tan, 2/13/1943","Bishop, Timothy, Male, Yellow, 4/23/1967","Kelly, Sue, Female, Pink, 7/12/1959"};

	InputFormatValidator ifv;
	string type;
	//put each type in dedicated container
	for (string personInfo : uknown_string_inputs) {
		ifv.validateInput(personInfo, type);
		if (type.compare(Commons::PIPE_TYPE) == 0) {
			pipe_string_inputs.push_back(personInfo);
		} else if (type.compare(Commons::SPACE_TYPE) == 0) {
			space_string_inputs.push_back(personInfo);
		} else if (type.compare(Commons::COMMA_TYPE) == 0) {
			comma_string_inputs.push_back(personInfo);
		}
	}

	//create parser for each type
	IParserFactory* parserFactory;
	IParser *parser;
	parserFactory = new ParserFactory;

	parser = parserFactory->createParser(Commons::PIPE_TYPE);
	vector<PersonInformation> pipe_persons = parser->parse(pipe_string_inputs);
	vector<PersonInformation>::iterator pipe_iter, pipe_end;
	parser = parserFactory->createParser(Commons::SPACE_TYPE);
	vector<PersonInformation> space_persons = parser->parse(space_string_inputs);

	parser = parserFactory->createParser(Commons::COMMA_TYPE);
	vector<PersonInformation> comma_persons = parser->parse(comma_string_inputs);

	//collect result in one container to be sorted
	vector<PersonInformation> all_persons;
	all_persons.reserve(pipe_persons.size() + space_persons.size() + comma_persons.size()); // preallocate memory
	all_persons.insert(all_persons.end(), pipe_persons.begin(),	pipe_persons.end());
	all_persons.insert(all_persons.end(), space_persons.begin(),space_persons.end());
	all_persons.insert(all_persons.end(), comma_persons.begin(),comma_persons.end());

	//sort result by gender (female first) then sort asc by last name
	GenderLastnameAscSort* _genderLastnameSorter = new GenderLastnameAscSort();
	_genderLastnameSorter->sortPersons(all_persons);

	//sort result by birth date asc and then last name asc
	BirthLastnameAscSort* _birthdatelastNameSorter = new BirthLastnameAscSort();
	_birthdatelastNameSorter->sortPersons(all_persons);

	//sort desc by last name first
	LastNameDescSort* _lastnameDescSorter = new LastNameDescSort();
	_lastnameDescSorter->sortPersons(all_persons);

	return Commons::SUCCESS;
}
