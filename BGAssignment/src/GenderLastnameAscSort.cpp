/*
 * GenderSort.cpp
 *
 *  Created on: Oct 3, 2016
 *      Author: ahmedasaleh
 *  Description : This file responsible for sorting by gender then by name asc
 */

#include "GenderLastnameAscSort.hpp"

namespace bgassignment {

GenderLastnameAscSort::GenderLastnameAscSort() {
	// TODO Auto-generated constructor stub

}

GenderLastnameAscSort::~GenderLastnameAscSort() {
	// TODO Auto-generated destructor stub
}

/*
 * @Description sort persons by gender putting females first
 * @param personsInformation (input) all persons data
 * @
 */
void GenderLastnameAscSort::sortPersons(vector<PersonInformation>  personsInformation){
	set<PersonInformation, LastNameAsc> _femalesOut1;
	set<PersonInformation, LastNameAsc> _malesOut1;
	set<PersonInformation, LastNameAsc>::iterator iter_l;
		vector<PersonInformation>::iterator all_iter,all_end;
		for(all_iter =  personsInformation.begin(),all_end = personsInformation.end();all_iter != all_end; ++all_iter){
			string _gender = (all_iter)->getGender();
			if(_gender.compare("Female")){
				map<string, string> _personMap; //single person information will be here
				(all_iter)->getPersonInformation(_personMap);
				PersonInformation _pi = PersonInformation(_personMap);
				_femalesOut1.insert(_pi);
			}
			else{
				map<string, string> _personMap; //single person information will be here
				(all_iter)->getPersonInformation(_personMap);
				PersonInformation _pi = PersonInformation(_personMap);
				_malesOut1.insert(_pi);
			}
		}
		cout<< "Output 1:\n\n";
		for (iter_l = _malesOut1.begin(); iter_l != _malesOut1.end(); ++iter_l) {
			std::cout << (iter_l)->printPersonInformation() << std::endl;
		}
		for (iter_l = _femalesOut1.begin(); iter_l != _femalesOut1.end(); ++iter_l) {
			std::cout << (iter_l)->printPersonInformation() << std::endl;
		}

}


} /* namespace bgassignment */
