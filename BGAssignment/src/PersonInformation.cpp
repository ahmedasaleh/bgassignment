/*
 * PersonInformation.cpp
 *
 *  Created on: Oct 3, 2016
 *      Author: ahmedasaleh
 *  Description : person entity class.
 */

#include "PersonInformation.hpp"

namespace bgassignment {

PersonInformation::PersonInformation() {
	_lastName.clear();
	_gender.clear();
	_dateOfBirth.clear();
	_middleInitial.clear();
	_firstName.clear();
	_favoriteColor.clear();
}

PersonInformation::PersonInformation(string lastName, string gender,
		string birthdate) {
	_lastName = lastName;
	_gender = reformatGender(gender);
	_dateOfBirth = reformatBirthDate(birthdate);
	_middleInitial.clear();
	_firstName.clear();
	_favoriteColor.clear();
}

PersonInformation::PersonInformation(string lastName, string firstName,
		string middleInitial, string gender, string birthdate,
		string favoriteColor) {
	_lastName = lastName;
	_gender = reformatGender(gender);
	_dateOfBirth = reformatBirthDate(birthdate);
	_middleInitial = middleInitial;
	_firstName = firstName;
	_favoriteColor = favoriteColor;
}

PersonInformation::PersonInformation(string lastName, string firstName,
		string gender, string birthdate, string favoriteColor) {
	_lastName = lastName;
	_gender = reformatGender(gender);
	_dateOfBirth = reformatBirthDate(birthdate);
	_middleInitial.clear();
	_firstName = firstName;
	_favoriteColor = favoriteColor;
}

PersonInformation::PersonInformation(map<string, string> personInformation) {
	map<string, string>::iterator iterator_person;
	PersonInformation person("","","");
	for (iterator_person = personInformation.begin();
			iterator_person != personInformation.end(); iterator_person++) {
		string key = iterator_person->first;
		string value = iterator_person->second;
		if (key.compare("lastName") ==0) {
			_lastName = value;
		} else if (key.compare("gender") ==0) {
			_gender = value;
		} else if (key.compare("dateOfBirth") ==0) {
			_dateOfBirth = value;
		} else if (key.compare("middleInitial")==0) {
			_middleInitial = value;
		} else if (key.compare("firstName")==0) {
			_firstName = value;
		} else if (key.compare("favoriteColor")==0) {
			_favoriteColor = value;
		}
	}
	person.printPersonInformation();
}

PersonInformation::~PersonInformation() {

}

string PersonInformation::reformatBirthDate(string birthDate) {
	string _formattedBirthdate = birthDate;
	std::replace(_formattedBirthdate.begin(), _formattedBirthdate.end(), '-',
			'/');
	return _formattedBirthdate;
}

void PersonInformation::getDateOfBirth(int &day,int &month,int &year) const{
	//int day_int =0,month_int=0,year_int=0;
	string s_day,s_month,s_year;
	string birth = this->_dateOfBirth;
	std::replace(birth.begin(), birth.end(), '/',' ' );
	stringstream ss(stringstream::in | stringstream::out);
	ss.str(birth);
	ss >> s_month >> s_day >>   s_year; //parse line to group of strings
	day = stoi(s_day);
	month = stoi(s_month);
	year = stoi(s_year);

}
string PersonInformation::reformatGender(string gender) {
	string _formattedGender = gender;
	if (_formattedGender.compare("M") == 0
			|| _formattedGender.compare("m") == 0) {
		_formattedGender = "Male";
	} else if (_formattedGender.compare("F") == 0
			|| _formattedGender.compare("f") == 0) {
		_formattedGender = "Female";
	}
	return _formattedGender;
}

string PersonInformation::printPersonInformation() const{
	string _personInfomation;
	_personInfomation = _lastName + _separator + _firstName + _separator
			+ _gender  + _separator + _dateOfBirth+ _separator
			+ _favoriteColor;
	return _personInfomation;
}

void PersonInformation::getPersonInformation (
		map<string, string> &personInformation) {
	_personInformation.insert( { "lastName", _lastName });
	_personInformation.insert( { "gender", _gender });
	_personInformation.insert( { "dateOfBirth", _dateOfBirth });
	_personInformation.insert( { "middleInitial", _middleInitial });
	_personInformation.insert( { "firstName", _firstName });
	_personInformation.insert( { "favoriteColor", _favoriteColor });
	personInformation = _personInformation;
}

void PersonInformation::setLastName(string lastName) {
	_lastName = lastName;
}
void PersonInformation::setFirstName(string firstName) {
	_firstName = firstName;
}
void PersonInformation::setGender(string gender) {
	_gender = gender;
}
void PersonInformation::setFavoriteColor(string favoriteColor) {
	_favoriteColor = favoriteColor;
}
void PersonInformation::setDateOfBirth(string dateOfBirth) {
	_dateOfBirth = dateOfBirth;
}
void PersonInformation::setMiddleInitial(string middleInitial) {
	_middleInitial = middleInitial;
}
string PersonInformation::getLastName() const {
	return _lastName;
}
string PersonInformation::getFirstName() const {
	return _firstName;
}
string PersonInformation::getGender() const {
	return _gender;
}
string PersonInformation::getFavoriteColor() const {
	return _favoriteColor;
}
string PersonInformation::getDateOfBirth() const {
	return _dateOfBirth;
}
string PersonInformation::getMiddleInitial() const {
	return _middleInitial;
}

bool PersonInformation::operator=(const PersonInformation &other) const {
	return (_gender.compare(other.getGender()) == 0);
}

} /* namespace bgassignment */
