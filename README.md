# README #

This is a sample source code for parsing stream of string inputs delimited by: pipe, comma, and space. Each type of them has format mostly similar to the other two types but with minor differences.


The pipe-delimited file lists each record as follows: LastName | FirstName | MiddleInitial | Gender | FavoriteColor | DateOfBirth

The comma-delimited file looks like this: LastName, FirstName, Gender, FavoriteColor, DateOfBirth

And lastly, the space-delimited file: LastName FirstName MiddleInitial Gender DateOfBirth FavoriteColor


This application parses those streams, and produce output in three formats:

Output 1: sorted by gender (females then males) then by last name ascending

Output 2: sorted by birth date ascending then by last name ascending

Output 3: sorted descending by last name

![BGAssignment.png](https://bitbucket.org/repo/AkrLkg/images/3164000126-BGAssignment.png)
### What is this repository for? ###

* Version 1.0

### How do I get set up? ###

* This source code is developed on Eclipse  Neon Milestone 4 (4.6.0M5) on Windows 7.
* This code uses c++11 features, to enable that on your Eclipse IDE: right click on the project and select Properties->C/C++ Build->Setting then select "Tools Settings" tab->Cygwin C++ Compiler->Miscellaneous in the "Other flags" input field append -std=c++0x .
* How to run tests


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: ahmedamsaleh@gmail.com
* open issue through issues tab